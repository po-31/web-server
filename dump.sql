-- MySQL dump 10.13  Distrib 5.7.9, for Win64 (x86_64)
--
-- Host: localhost    Database: gps
-- ------------------------------------------------------
-- Server version	5.7.13-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `coord`
--

DROP TABLE IF EXISTS `coord`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `coord` (
  `iduser` int(11) NOT NULL,
  `date` datetime NOT NULL,
  `coordx` double NOT NULL,
  `coordy` double NOT NULL,
  PRIMARY KEY (`iduser`,`date`),
  CONSTRAINT `fk_iduser` FOREIGN KEY (`iduser`) REFERENCES `user` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `coord`
--

LOCK TABLES `coord` WRITE;
/*!40000 ALTER TABLE `coord` DISABLE KEYS */;
INSERT INTO `coord` VALUES (1,'2016-09-01 00:00:00',48.5027313,135.06625989999998),(1,'2016-09-02 00:00:00',48.6227313,135.22625989999997),(1,'2016-09-03 00:00:00',48.6027313,135.21625989999998),(1,'2016-09-04 00:00:00',48.6627313,135.24625989999998),(1,'2016-09-05 00:00:00',48.6227313,135.18625989999998),(1,'2016-09-06 00:00:00',48.7727313,135.28625989999998),(1,'2016-09-07 00:00:00',48.6627313,135.27625989999999),(1,'2016-09-08 00:00:00',48.8127313,135.32625989999997),(1,'2016-09-09 00:00:00',48.6327313,135.26625989999997),(1,'2016-09-10 00:00:00',48.6927313,135.29625989999997),(1,'2016-09-11 00:00:00',48.5927313,135.04625989999997),(1,'2016-10-01 00:00:00',48.5027313,135.06625989999998),(1,'2016-10-02 00:00:00',48.90643677771454,134.26833347406614),(1,'2016-10-03 00:00:00',48.45122793972855,134.57565543976057),(1,'2016-10-04 00:00:00',49.48418924868469,134.39377740946043),(1,'2016-10-05 00:00:00',48.111044920090485,134.59994969692457),(1,'2016-10-06 00:00:00',48.76344660580521,134.38576356497038),(1,'2016-10-07 00:00:00',49.25320086539154,136.06602326956022),(1,'2016-10-08 00:00:00',47.61828533470993,135.70162335958707),(1,'2016-10-09 00:00:00',47.70174542725449,135.27606199112165),(1,'2016-10-10 00:00:00',48.18879053652649,134.130907157328),(1,'2016-10-11 00:00:00',48.77143291294937,135.03319136228788),(1,'2016-10-12 00:00:00',48.67050740778809,135.6545605453514);
/*!40000 ALTER TABLE `coord` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `group`
--

DROP TABLE IF EXISTS `group`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `group` (
  `group` varchar(14) NOT NULL,
  PRIMARY KEY (`group`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `group`
--

LOCK TABLES `group` WRITE;
/*!40000 ALTER TABLE `group` DISABLE KEYS */;
INSERT INTO `group` VALUES ('user');
/*!40000 ALTER TABLE `group` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user`
--

DROP TABLE IF EXISTS `user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `number` varchar(15) NOT NULL,
  `password` varchar(45) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `number_UNIQUE` (`number`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user`
--

LOCK TABLES `user` WRITE;
/*!40000 ALTER TABLE `user` DISABLE KEYS */;
INSERT INTO `user` VALUES (1,'000000000000001','5f4dcc3b5aa765d61d8327deb882cf99'),(2,'(929) 410-0722','7c6a180b36896a0a8c02787eeafb0e4c');
/*!40000 ALTER TABLE `user` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user_group`
--

DROP TABLE IF EXISTS `user_group`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user_group` (
  `number` varchar(15) NOT NULL,
  `group` varchar(14) NOT NULL,
  KEY `fk_numbe_idx` (`number`),
  KEY `fk_group_idx` (`group`),
  CONSTRAINT `fk_group` FOREIGN KEY (`group`) REFERENCES `group` (`group`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_number` FOREIGN KEY (`number`) REFERENCES `user` (`number`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user_group`
--

LOCK TABLES `user_group` WRITE;
/*!40000 ALTER TABLE `user_group` DISABLE KEYS */;
INSERT INTO `user_group` VALUES ('000000000000001','user');
/*!40000 ALTER TABLE `user_group` ENABLE KEYS */;
UNLOCK TABLES;
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                  