package gps.view;
     
import gps.beans.Coord;
import gps.db.DataBase;
import java.io.Serializable;
import java.util.Date;

import java.util.List;
import javax.annotation.PostConstruct; 
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import org.primefaces.event.SelectEvent;
import org.primefaces.model.map.DefaultMapModel;
import org.primefaces.model.map.LatLng;
import org.primefaces.model.map.MapModel;
import org.primefaces.model.map.Polyline;
  
@ManagedBean
@SessionScoped
public class PolylinesView implements Serializable {
  
    private MapModel polylineModel;
    private String centerGeoMap = "36.890257,30.707417";
    private Date firstDate;
    private Date lastDate;
  
    @PostConstruct
    public void init() {
        lastDate = new Date();
        firstDate = new Date();
        firstDate.setDate(firstDate.getDate() - 1);
        updateMap(null);
    }
    
    public void updateMap(SelectEvent event) {
        List<Coord> coords = DataBase.getCoords(firstDate, lastDate);
        
        polylineModel = new DefaultMapModel();
        
        Polyline polyline = new Polyline();
        for(Coord coord : coords)
            polyline.getPaths().add(new LatLng(coord.getX(), coord.getY()));
        if(coords.size() > 0) centerGeoMap = coords.get(0).toString();
          
        polyline.setStrokeWeight(10);
        polyline.setStrokeColor("#FF9900");
        polyline.setStrokeOpacity(0.7);
          
        polylineModel.addOverlay(polyline);
//        polylineModel.addOverlay(new Marker(new LatLng(coords.get(coords.size() - 1).getX(), coords.get(coords.size() - 1).getY())));
    }
  
    public MapModel getPolylineModel() {
        return polylineModel;
    }

    public String getCenterGeoMap() {
        return centerGeoMap;
    }

    public Date getFirstDate() {
        return firstDate;
    }

    public Date getLastDate() {
        return lastDate;
    }

    public void setFirstDate(Date firstDate) {
        this.firstDate = firstDate;
    }

    public void setLastDate(Date lastDate) {
        this.lastDate = lastDate;
    }
    
}