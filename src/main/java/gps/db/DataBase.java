
package gps.db;

import gps.beans.Coord;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.faces.context.FacesContext;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.sql.DataSource;

public class DataBase {
    
    private static SimpleDateFormat dateFormate = new SimpleDateFormat("yyyy-MM-dd HH:00:00");
    
    public static Connection getConnection(){
        Connection conn = null;
        try{
            InitialContext ic = new InitialContext();
            DataSource ds = (DataSource) ic.lookup("jdbc/gps");
            conn = ds.getConnection();
        } catch (NamingException ex) {
            Logger.getLogger(DataBase.class.getName()).log(Level.SEVERE, null, ex);
        } catch (SQLException ex) {
            Logger.getLogger(DataBase.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        return conn;
    }
    
    public static List<Coord> getCoords(Date firstDate, Date lastDate){
        List<Coord> result = new ArrayList<Coord>();
        try {
            Connection conn = getConnection();
            Statement stmt = conn.createStatement();
            String username = FacesContext.getCurrentInstance().getExternalContext().getUserPrincipal().getName();
            
            ResultSet rs = stmt.executeQuery(String.format("SELECT coordx, coordy, date FROM coord LEFT JOIN user ON "
                    + "coord.iduser = user.id WHERE number = '%s' AND date BETWEEN '%s' AND '%s'", username, 
                    dateFormate.format(firstDate.getTime()), dateFormate.format(lastDate.getTime())));
            while(rs.next()) result.add(new Coord(rs.getDouble("coordx"), rs.getDouble("coordy")));
            rs.close();
            stmt.close();
            conn.close();
        } catch (SQLException ex) {
            Logger.getLogger(DataBase.class.getName()).log(Level.SEVERE, null, ex);
        }
        return result;
    }
}
